//
//  deviceDelegate.h
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-08-10.
//

//@protocol deviceDelegate <NSObject>
//@optional
//- (void) onDeviceReachableStatusChanged:(Device*)device;
//- (void) onDevicePairRequest:(Device*)device;
//- (void) onDevicePairTimeout:(Device*)device;
//- (void) onDevicePairSuccess:(Device*)device;
//- (void) onDevicePairRejected:(Device*)device;
//- (void) onDevicePluginChanged:(Device*)device;
//@end
